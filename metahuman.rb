class Metahuman
	attr_accessor :real_name, :screen_name, :powers, :hitpoints, :team


	def initialize(real_name, screen_name, powers, hp, team)
		@real_name = real_name
		@screen_name = screen_name
		@powers = powers
		@hitpoints = hp
		@team = team
	end

	def real_name=(rname)
		@real_name = rname
	end

	def screen_name=(sname)
		@screen_name = sname
	end

	def powers=(new_pow)
		@powers = new_pow
	end

	def hp=(nhp)
		@hitpoints = nhp
	end

	def to_s
		puts "#{@screen_name}"
	end

	def valid_power?(power)
		@powers.keys.include?(power)
	end

	def is_dead?
		@hitpoints <= 0
	end

	def same_team?(meta)
		@team == meta.team
	end

end
