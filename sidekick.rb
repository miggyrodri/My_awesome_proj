require './metahuman'

class Sidekick < Metahuman
	attr_accessor :leader
	def initialize(real_name, screen_name, powers, hp, team, leader)
		@real_name = real_name
		@screen_name = screen_name
		@powers = powers
		@hitpoints = hp
		@team = team
		@leader = leader
	end

	def can_attack?(meta)
		if same_team?(meta)
			if meta.class == SuperHero
				return false
			elsif meta.class == Sidekick
				return false
			else 
				return true
			end
		else
			return true
		end
	end

	def attack(meta, power)
		if valid_power?(power)
			if can_attack?(meta)
				if meta.is_dead?
					puts "#{meta.screen_name} is already dead."
				else
					if @leader.is_dead?
						meta.hp= meta.hitpoints - (@powers[power] * 2)
						puts "#{@screen_name} avenged #{@leader.screen_name} and hit #{meta.screen_name} with #{power}"
					else 
						meta.hp= meta.hitpoints - @powers[power]
						puts "#{@screen_name} hit  #{meta.screen_name} with #{power}"
					end
				end
			else
				puts "#{@screen_name} cannot hit #{meta.screen_name}"
			end
		else
			puts "#{meta.screen_name} does not have that power"
		end
	end
end
