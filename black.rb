class Black

	def initialize(watermelon, fried_chicken)
		@watermelon = watermelon
		@fried_chicken = fried_chicken
	end

	def add_water(water)
		@watermelon << water
	end
end