require './metahuman'

class Villain < Metahuman
	attr_accessor :archenemy
	def initialize(real_name, screen_name, powers, hp, team, archenemy)
		@real_name = real_name
		@screen_name = screen_name
		@powers = powers
		@hitpoints = hp
		@team = team
		@archenemy = archenemy
	end

	def can_attack?(meta)
		true
	end

	def attack(meta, power)
		if valid_power?(power)
			if can_attack?(meta)
				if meta.is_dead?
					puts "#{meta.screen_name} is already dead."
				else
					if meta == @archenemy
						meta.hp= meta.hitpoints - (@powers[power] * 2)
						puts "#{@screen_name} hit his archenemy, #{meta.screen_name} with #{power}"
					else 
						meta.hp= meta.hitpoints - @powers[power]
						puts "#{@screen_name} hit  #{meta.screen_name} with #{power}"
					end
				end
			else
				puts "#{@screen_name} cannot hit #{meta.screen_name}"
			end
		else
			puts "#{meta.screen_name} does not have that power"
		end
	end
end
